# -*- coding: utf-8 *-*
from setuptools import setup

setup(
    name='Calamansi',
    version='0.1',
    long_description=__doc__,
    packages=['calamansi'],
    url='http://bitbucket.org/romabysen/calamansi',
    scripts=['calamansi-server.py'],
    author='Lars Hansson',
    author_email='romabysen@gmail.com',
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Flask',
        'WTForms'
    ],
    classifiers=[
        'Programming Language :: Python',
        'Environment :: Web Environment',
        'Operating System :: OS Independent',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: BSD License'
        ]
)
