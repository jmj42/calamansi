# -*- coding: utf-8 -*-
"""
Configuration defaults
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""
import os
import os.path


SECRET_KEY = os.urandom(24)
# Enable/disable debugging
DEBUG = False

#: Address to use for websockets.
WSADDRESS = '0.0.0.0'

# Path to websockify
_mypath = os.path.dirname(os.path.realpath(__file__))
WSEXEC = os.path.join(_mypath, 'websockify', 'websockify')

## Path to database file
DATABASE = None

# Set to True if behind a reverse proxy
REVERSE_PROXY = False

# For secure websockets
CERTFILE = None
KEYFILE = None 
