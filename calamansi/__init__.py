# -*- coding: utf-8 -*-
"""
Web frontend for libvirt.
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""

from flask import Flask, g

# create application
app = Flask('calamansi', instance_relative_config=True)
app.config.from_object('calamansi.default_settings')
app.config.from_envvar('CALAMANSI_SETTINGS', silent=True)

if app.config['REVERSE_PROXY']:
    from calamansi.util import ReverseProxied
    app.wsgi_app = ReverseProxied(app.wsgi_app)

import calamansi.views
import calamansi.ajax
from calamansi import db


@app.before_request
def before_request():
    g.db = db.Db(app.config['DATABASE'], app.config['TESTING'], app.logger)
    if app.config['DATABASE'] is None:
        g.standalone = True
    else:
        g.standalone = False
    g.appname = app.name.capitalize()


@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()


if __name__ == '__main__':
    app.run()
