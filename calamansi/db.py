# -*- coding: utf-8 -*-
"""
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""
import os
import gdbm 
from flask import json

from calamansi import virtutil 

class Db(object):
    def __init__(self, dbpath, testing, logger=None):
        self.dbpath = dbpath
        if self.dbpath is None:
            self._local_name = None
            if logger is not None:
                logger.info('Database not configured. Autodetecting local hypervisor')
            (self._local_name, self._local_uri) = virtutil.detect_local()
            if self._local_name is None:
                if logger is not None:
                    logger.info('No local hypervisor found. Will use test settings')
                self._local_name = 'test'
                self._local_data = {'uri': 'test:///default', 'username': None, 'password': None}
            else:
                if logger is not None:
                    logger.info('Found local hypervisor at %s' % (self._local_uri))
                self._local_data = {'uri': self._local_uri, 'username': None, 'password': None}
        else:
            # Create the database file if it does not exist
            if not os.path.exists(self.dbpath) or testing:
                db = gdbm.open(self.dbpath, 'c')
                db.close()
            self.db = gdbm.open(self.dbpath, 'ru')
    def close(self):
        if self.dbpath is not None:
            self.db.close()
    def get_hosts(self):
        """Return a list of configured hosts"""
        if self.dbpath is not None:
            return self.db.keys()
        else:
            return [self._local_name]
    def get_hostconfig(self, host):
        """Get config of host"""
        if self.dbpath is not None:
            r = json.loads(self.db[host])
            if not 'username' in r:
                r['username'] = None
            if not 'password' in r:
                r['password'] = None
            return r
        else:
            return self._local_data
    def save_host(self, hostname, uri, username, password):
        d = {'uri': uri, 'username': username, 'password': password}
        j = json.dumps(d)
        if self.dbpath is not None:
            db = gdbm.open(self.dbpath, 'wsu')
            db[hostname] = j
            db.close()
    def remove_host(self, hostname):
        db = gdbm.open(self.dbpath, 'wsu')
        del db[hostname]
