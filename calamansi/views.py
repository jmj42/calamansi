# -*- coding: utf-8 -*-
"""
Views
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""

from flask import abort, request, redirect, url_for
from flask import render_template, g

from calamansi import app, virtutil, db, forms


@app.route('/')
def hosts():
    hostlist = []
    hlist = g.db.get_hosts()
    for host in hlist:
        conf = g.db.get_hostconfig(host)
        info = {}
        info['name'] = host
        info['uri'] = conf['uri']
        try:
            conn = virtutil.connect_host(conf)
        except:
            app.logger.error('Error connecting to host: %s' % (host))
            info['domain'] = []
        else:
            info['domains'] = virtutil.get_alldomains(conn)
        finally:
            hostlist.append(info)
            try:
                conn.close()
            except:
                pass
    return render_template('hosts.html', hostlist=hostlist)


@app.route('/addhost', methods=['GET', 'POST'])
def addhost():
    form = forms.HostForm(request.form)
    if request.method == 'POST' and form.delete.data:
        g.db.remove_host(form.hostname.data)
        return redirect(url_for('hosts'))
    elif request.method == 'POST' and form.validate():
        hostname = form.hostname.data
        uri = form.uri.data
        username = form.username.data
        if username.strip() == '':
            username = None
        password = form.password.data
        if password.strip() == '':
            password = None
        g.db.save_host(hostname, uri, username, password)
        return redirect(url_for('hosts'))
    return render_template('edit_host.html', form=form)


@app.route('/host/<host>')
def hostinfo(host):
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(404)
    try:
        conn = virtutil.connect_host(conf)
    except:
        app.logger.error('Error connecting to host: %s' % (host))
        return redirect(url_for('edithost', host=host))
    host = virtutil.HostInfo(conn, host)
    host.uri = conf['uri']
    domains = virtutil.get_alldomains(conn)
    conn.close()
    return render_template('hostinfo.html', host=host, domains=domains)


@app.route('/host/<host>/edit')
def edithost(host):
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(400)
    username = conf['username']
    if username is None:
        username = ''
    password = conf['password']
    if password is None:
        password = ''
    form = forms.HostForm(request.form, hostname=host, uri=conf['uri'],
                        username=username, password=password)
    return render_template('edit_host.html', form=form)


@app.route('/host/<host>/vm/<domname>')
def domaininfo(host, domname):
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(400)
    try:
        conn = virtutil.connect_host(conf)
    except:
        app.logger.error('Error connecting to host: %s' % (host))
        abort(500)
    domaininfo = virtutil.get_domain(conn, domname)
    conn.close()
    return render_template('domaininfo.html', domain=domaininfo, host=host)


@app.route('/host/<host>/vm/<domname>/console')
def domainconsole(host, domname):
    if host not in g.db.get_hosts():
        app.logger.error('Unknown host: %s' % (host))
        abort(400)
    else:
        return render_template('vnc.html', host=host, domain=domname,)
