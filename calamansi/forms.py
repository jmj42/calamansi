# -*- coding: utf-8 *-*
"""
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""
from wtforms import Form, PasswordField, TextField, SubmitField, validators


class HostForm(Form):
    hostname = TextField('Hostname', [validators.Length(min=1)])
    uri = TextField('URI', [validators.Length(min=1)])
    username = TextField('Username')
    password = PasswordField('Password')
    save = SubmitField()
    delete = SubmitField()
