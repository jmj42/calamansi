# -*- coding: utf-8 -*-
"""
Utility functions and classes for libvirt.
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""

from xml.etree import ElementTree
import socket

import libvirt

domain_states = {
    0: 'no_state',
    1: 'running',
    2: 'blocked',
    3: 'paused',
    4: 'shutting_down',
    5: 'off',
    6: 'crashed'
    }


# Classes
class HostInfo(object):
    """Helper class for host information"""
    def __init__(self, conn, host):
        self.shortname = host
        self.htype = conn.getType()
        self.libver = conn.getLibVersion()
        try:
            self.ver = conn.getVersion()
        except AttributeError:
            self.ver = None
        self.hostname = conn.getHostname()
        self.running = conn.numOfDomains()
        self.total = conn.numOfDomains() + conn.numOfDefinedDomains()


class DomainInfo(object):
    """Helper class for domain information"""
    def __init__(self, domain):
        info = domain.info()
        self.ostype = domain.OSType()
        self.name = domain.name()
        self.uuid = domain.UUIDString()
        self.autostart = domain.autostart()
        self.mem = info[2]
        self.maxmem = info[1]
        self.state = info[0]
        self.state_str = domain_states[info[0]]
        etree = ElementTree.fromstring(domain.XMLDesc(0))
        devs = etree.find('devices')
        self.vnc_port = None
        for dev in devs.findall('graphics'):
            self.vnc_port = dev.get('port')
        de = etree.find('description')
        if de is not None:
            self.description = de.text


# Functions
def detect_local():
    """Detect the local hypervisor, if any"""
    for item in ['qemu:///system', 'qemu+tcp://127.0.0.1/system']:
        try:
            r = libvirt.open(item)
        except:
            pass
        else:
            r.close()
            name = socket.gethostname()
            return (name, item)
    return (None, None)


def get_domain(conn, domain):
    d = conn.lookupByName(domain)
    return DomainInfo(d)


def get_alldomains(conn):
    """List of all domains on the host"""
    return get_activedomains(conn) + get_inactivedomains(conn)


def get_activedomains(conn):
    """
    Return a list of DomainInfo objects representing the active domains.
    """
    activedomains = []
    for d_id in conn.listDomainsID():
        domain = conn.lookupByID(d_id)
        activedomains.append(DomainInfo(domain))
    return activedomains


def get_inactivedomains(conn):
    """
    Return a list of DomainInfo objects representing the inactive domains.
    """
    inactivedomains = []
    for name in conn.listDefinedDomains():
        domain = conn.lookupByName(name)
        inactivedomains.append(DomainInfo(domain))
    return inactivedomains


def _request_credentials(credentials, user_data):
    """Authenticate with host"""
    for credential in credentials:
        if credential[0] in [libvirt.VIR_CRED_USERNAME, libvirt.VIR_CRED_AUTHNAME]:
            credential[4] = user_data[0]
        elif credential[0] == libvirt.VIR_CRED_PASSPHRASE:
            credential[4] = user_data[1]
        else:
            return -1

    return 0


def connect_host(conf, readonly=True):
    username = conf['username']
    password = conf['password']
    uri = conf['uri']
    auth = [[libvirt.VIR_CRED_USERNAME, libvirt.VIR_CRED_AUTHNAME,
            libvirt.VIR_CRED_PASSPHRASE], _request_credentials, (username, password)]
    if username is None:
        if readonly:
            return libvirt.openReadOnly(uri)
        else:
            return libvirt.open(uri)
    else:
        flags = 0
        if readonly:
            flags = libvirt.VIR_CONNECT_RO
        return libvirt.openAuth(uri, auth, flags)
