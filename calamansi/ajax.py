# -*- coding: utf-8 -*-
"""
AJAX functions
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""

import subprocess
import urlparse
import sys

import libvirt
from flask import request, abort, jsonify, g

from calamansi import app, virtutil, util, db


@app.route('/host/<host>/vm/<domain>/console-info')
def domain_consoleinfo(host, domain):
    """Spawn websockify and return host:port"""
    args = [sys.executable, app.config['WSEXEC'], '--daemon', '--run-once', '--timeout=5']
    if app.config['CERTFILE'] is not None and app.config['KEYFILE'] is not None:
        args.append('--cert=%s' % (app.config['CERTFILE']))
        args.append('--key=%s' % (app.config['KEYFILE']))
    l_host = app.config['WSADDRESS']
    l_port = util.PickUnusedPort(l_host)
    hostheader = request.headers.get('Host', None)
    if hostheader is None:
        abort(500)
    c_host = hostheader.split(':')[0]
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(404)
    t_host = urlparse.urlparse(conf['uri']).hostname
    if t_host is None:
        t_host = '127.0.0.1'
    conn = virtutil.connect_host(conf)
    try:
        dominfo = virtutil.get_domain(conn, domain)
    except libvirt.libvirtError:
        app.logger.error('Error looking up domain: %s' % (domain))
        conn.close()
        abort(404)
    conn.close()
    if dominfo.vnc_port is None:
        if app.config['TESTING']:
            dominfo.vnc_port = 5900 
        else:
            app.logger.warning('Domain has no VNC console')
            abort(500)
    t_port = dominfo.vnc_port
    args.append('%s:%s' % (l_host, l_port))
    args.append('%s:%s' % (t_host, t_port))
    proc = subprocess.Popen(args)
    # Wait for websockify to daemonize
    if proc.wait() != 0:
        app.logger.error('Websockify error')
        abort(500)
    else:
        return jsonify(host=c_host, port=l_port)


@app.route('/host/<host>/vm/<domain>/control')
def domain_control(host, domain):
    """Control domains"""
    action = request.args.get('action', None)
    if action is None:
        app.logger.error('No action given')
        abort(400)
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(404)
    conn = virtutil.connect_host(conf, readonly=False)
    try:
        domobj = conn.lookupByName(domain)
    except libvirt.libvirtError:
        app.logger.error('Error looking up domain: %s' % (domain))
        conn.close()
        abort(404)
    if action == 'stop':
        msg = 'Stop initiated for %s ' % (domain)
        domobj.shutdown()
    elif action == 'force_stop':
        msg = 'Forced stop initiated for %s ' % (domain)
        domobj.destroy()
    elif action == 'reboot':
        msg = 'Reboot initiated for %s ' % (domain)
        domobj.reboot(0)
    elif action == 'start':
        msg = 'Start initiated for %s' % (domain)
        domobj.create()
    else:
        abort(400)
    conn.close()
    return jsonify(msg=msg)


@app.route('/host/<host>/vm/<domain>/get-state')
def domain_state(host, domain):
    try:
        conf = g.db.get_hostconfig(host)
    except KeyError:
        app.logger.error('Unknown host: %s' % (host))
        abort(404)
    conn = virtutil.connect_host(conf, readonly=False)
    try:
        dinfo = virtutil.get_domain(conn, domain)
    except libvirt.libvirtError:
        app.logger.error('Error looking up domain: %s' % (domain))
        conn.close()
        abort(404)
    return jsonify(state=dinfo.state, state_str=dinfo.state_str)
