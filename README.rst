About
-----
Calamansi is a web frontend for libvirt. It is designed to be easy to use, install and configure.

Features
--------
  * Start,stop and reboot virtual machines
  * Access the virtual machine consoles
  * Access multiple hosts from the same web interface

Installation
------------
Requirements
++++++++++++
  * Python >= 2.5
  * Flask >= 0.9
  * WTForms >= 1.0
  * libvirt python bindings
  * numpy (optional), improves VNC console performance

Calamansi makes use of html5 and css3 features so you need a reasonably recent browser. Any IE before 9 is going to have problems.

Installation
++++++++++++
It is strongly recommended to install into a virtualenv. libvirt is not available via pypi so you need to install that some other way, preferably a distro package. numpy can be installed at any time before or after calamansi. To install Calamansi just do:

  $ python setup.py install

Calamansi can also be run directly from the source directory if all requirements are met.

Libvirt
+++++++
Currently only qemu and qemu+tcp connections are supported and tested. For automatic configuration to work the user running Calamansi need to have access to the libvirt socket file or libvirt should accept unauthenticated TCP connections on 127.0.0.1. For the VNC console to work qemu need to be confgiured to allow remote access to the VNC consoles. For managing multiple hosts the libvirt on each host need to be configured to accept TCP connections.

Configuration
-------------
Environment variables
+++++++++++++++++++++
CALAMANSI_SETTINGS
  This environment variable sets the path to the configuration file. If the path to the configuration file is relative it will be loaded using the instance folder feature.

Command-line options
++++++++++++++++++++
-b  Listen address. Default 127.0.0.1.
-p  Listen port. Default 5000.

Configuration file options
++++++++++++++++++++++++++
The configuration file is a valid python asource file and you assign options with standard python syntax. Most likely this will only be a matter of "OPTION = Value" but anything is possible. 

DEBUG
  Turn on debugging.

SECRET_KEY
  Secret key. If not set defaults to a 24 character random string.

DATABASE
  Path to the database file to use for storing host settings. By default no database is configured and the local host will be autodetected. 

WSEXEC
  Path to the websockify script. This is automatically detected and you shouldn't need to set this.

WSADDRESS
  The address to use for the websocket proxy. It defaults to '0.0.0.0'.

REVERSE_PROXY
  If you run Calamansi behind a reverse proxy, like nginx, you need to set this to True.

CERTFILE
  The certificate to use for secure websockets. If you're serving Calmansi over https you need to set this (and KEYFILE) in order for the consoles to work. Calamansi will automatically use secure websockets when served from an https:// url. It's advisable to use the same certificate and key as for the webserver.

KEYFILE
  The key to use for secure websockets.

Running
-------
Simple stand-alone server
+++++++++++++++++++++++++
To run the stand-alone server::

  $ calamansi-server

This will run Calamansi in stand-alone mode using the built-in web server and this is usually sufficient for small installations. This is suitable for installing on the virtualization host itself. No configuration is necessary to get up and running.

Aside from the stand-alone server Calamansi can be deployed using any of the means available to wsgi applications. A few examples are shown below.

With gunicorn
+++++++++++++
In your virtualenv for Calamansi::

  $ pip install gunicorn
  $ CALAMANSI_SETTINGS="/path/to/settings" gunicorn calamansi:app

With nginx reverse proxy
++++++++++++++++++++++++
Run calamansi standalone or with gunicorn. Set REVERSE_PROXY = True in the configuration file.
Nginx configuration to serve calamansi under the /calamansi prefix::

    location /calamansi {
        proxy_pass http://127.0.0.1:5000;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /calamansi;
    }

More hosts
----------
In the simplest set-up Calamansi will autodetect the local hypervisor and configure itself to use it.
It is also possible to use Calamansi to manage multiple hosts. For this you need to use a configuration file to configure a database.
Once this is done you use the "Add host" link to add hosts.

Copyright
---------
| Calamansi is copyright 2012 Lars Hansson and licensed under the BSD license. See LICENSE.
| noVNC and websockify are copyright Joel Martin and licensed under LGPL version 3.
| jquery is copyright John Resig and dual-licensed under the MIT and GPL licenses.
| jquery-ui is copyright by many (see http://jqueryui.com/about) and dual-licensed under the MIT and GPL licenses.
| jquery-ui dialogWrapper is copyright Chris Laplante and licensed under the MIT license.
| jgrowl is copyright Stan Lemon and dual-licensed under the MIT and GPL licenses.
