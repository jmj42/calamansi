#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Stand-alone server for Calamansi
Copyright: (c) 2012 Lars Hansson.
License: BSD, see LICENSE for more details.
"""

import os
from optparse import OptionParser
from calamansi import app

def handle_options():
    parser = OptionParser()
    parser.add_option('-b', dest='address', default='127.0.0.1',
                    help='Address to listen on (Default: %default)')
    parser.add_option('-p', dest='port', type='int', default=5000,
                    help='Port to listen on (Default: %default)')
    return parser.parse_args()


def main():
    (opts, args) = handle_options()
    app.run(host=opts.address, port=opts.port)


if __name__ == '__main__':
    main()

