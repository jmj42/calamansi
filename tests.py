#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unit tests
Copyright: (c) 2012 Lars Hansson.
License: BSD.
"""
import os
import unittest
import tempfile

import calamansi


class StandaloneTestCase(unittest.TestCase):
    def setUp(self):
        #self.db_fd, minitwit.app.config['DATABASE'] = tempfile.mkstemp()
        self.db_fd, calamansi.app.config['DATABASE'] = tempfile.mkstemp(suffix=".db")
        calamansi.app.config['TESTING'] = True
        self.app = calamansi.app.test_client()
    def tearDown(self):
        """Get rid of the database again after each test."""
        os.close(self.db_fd)
        os.unlink(calamansi.app.config['DATABASE'])
    # Utility functions
    def add_test_host(self): 
        db = calamansi.db.Db(calamansi.app.config['DATABASE'], True)
        db.save_host('test','test:///default' , None, None)
    # Tests
    def test_frontpage(self):
        rv = self.app.get('/')
        assert '<a href="/host/test">test</a>' not in rv.data
        self.add_test_host()
        rv = self.app.get('/')
        assert '<a href="/host/test">test</a>' in rv.data
    def test_hostpage(self):
        self.add_test_host()
        rv = self.app.get('/host/test')
        assert '<tr><td class="infotitle">Connection:</td><td>test:///default</td></tr>' in rv.data
    def test_vmpage(self):
        self.add_test_host()
        rv = self.app.get('/host/test/vm/test')
        assert '<tr><td class="infotitle">State:</td><td id="state">running</td></tr>' in rv.data
        assert '<tr><td class="infotitle">Hosted on:</td><td><a href="/host/test">test</a></td></tr>' in rv.data
    def test_ajax_consoleinfo(self):
        self.add_test_host()
        rv = self.app.get('/host/test/vm/test/console-info')
        assert '"host"' in rv.data
        assert '"port"' in rv.data
    def test_ajax_domainstate(self):
        self.add_test_host()
        rv = self.app.get('/host/test/vm/test/get-state')
        assert '"state"' in rv.data
        assert '"state_str"' in rv.data
    def test_ajax_domaincontrol(self):
        self.add_test_host()
        rv = self.app.get('/host/test/vm/test/control')
        # if not action is given we should get a 400 error
        assert rv.status_code == 400
        rv = self.app.get('/host/test/vm/test/control', query_string={'action': 'stop'})
        assert '"msg": "Stop initiated for test "' in rv.data


if __name__ == '__main__':
    unittest.main()
